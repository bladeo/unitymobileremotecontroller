User Manual
BitBucket repository at: https://bitbucket.org/bladeo/unitymobileremotecontroller/src
This manual is intended to assist users with a moderate understanding of unity to install and use the mobile controller in their own projects.

Using the mobile controller
The following software is needed to use following items in the repository:
•	To open the unity projects- Unity version 5.3.5f1 or above
•	To install the ClientBuilt.apk- android version 2.3 or above
•	To use the scripts available in the ServerPackage- Unity version 5 or above
•	Server and client running on the same network

Installing the client and creating builds
Opening the ClientBuilt.apk on an android device will install the client automatically if the install if unknown sources option in the settings is enabled.
If an edit to the client is required, the client project can be opened using Unity and builds created manually, however this will require the prerequisite modules to build to target platforms to be installed to unity as required.

Using the client
1.	Install and open the client
2.	Fill IP field with the IP on the screen when the server is started
3.	Start the client
No further action is required. If a connection is established the server will request for the client to use send specific data which can be managed using the server dataRequest variables. 

Installing and attaching the server scripts
To import the ServerPackage using Unity open the package using Assests>Import package> Custom Package. The server can be built to most target platforms including android allowing for one device to be used as a screen and another as a controller.
The Server project on the repository gives an example of how to set up the server. Any scripts that require the Server Script Holder should have the gameObject containing the server script attatched.
-Server script: The server script can be attached to any object that will be active when the server is required to be active. The server has the following pre-sets that can be set in the inspector:
1.	Startup Vector Data Enabled- enabling this option enables and requests vector rotation data from the controller. The script VectorRotation can be partnered with this option to use this data.

2.	Startup Accelerometer Enabled- enabling this option enables the controllers’ accelerometer and requests this data. This can be partnered with the Accelerometer script to pre-process this data smoothening results and making data more reliable.

3.	Startup Gyro Enabled- Enabling this option requests the Gyroscope data from the device. This data can be used with the Gyroscope script.

4.	Startup Buttons Enabled- Enabling this option creates two buttons on the client device. The functionality of these buttons can be controlled using the Buttons script.

5.	Startup Ping Enabled- Enabling this option shows the ping of the device on screen. The ping is updated every one second.

6.	Startup Target Fps- This option allows for the target framerate of the server project and controller to be set. A lower target Fps can produce smoother results on lower spec devices however a target framerate below 30 Fps may produce noticeably slower results.

- VectorRotation script- This script by default is designed to be attached to a camera. The script rotates the camera to give the effect of looking through the camera on the device. However this script can be edited for more specific needs.
-Accelerometer script- This script by default can be attached to any object to be moved by the accelerometer of the device. The script by default only uses the x and y axis however the z can be enabled by editing the script as instructed by the code comments.
-Buttons- This script allows the buttons to be pressed and held. The functionality of these buttons can easily be added to the script in the spaces allocated.
-Gyroscope- This script allows access to the gyroscope data from the device which can be used as required.

Customising server options
To access public server variables and functions from other scripts containing the serverScriptHolder GameObject the following can be used:
serverScriptHolder.GetComponent<ServerScript>(). //Function or variable name
•	Test if device is connected- It can be tested if a device is connected using the public variable “deviceConnected”. This will be updated if a device connects or disconnects.

•	Disconnect served- To disconnect the server the function “DisconnectServer” can be called, disconnecting all clients and shutting down the server.

•	Changing data request (used to request or stop requesting device data)- A data request is sent at start-up using the start-up variables listed above. However a data request can be sent during run-time by editing the dataMessage.dataRequest array as follows:
dataMessage.dataRequest[0] = set true to enable vector rotation data.
dataMessage.dataRequest[1] = set true to accelerometer data.
dataMessage.dataRequest[2] = set true to enable gyroscope data.
dataMessage.dataRequest[3] = set true to enable buttons.

The target framerate can also be changed using the following:
dataMessage.targetFps = target framerate
The data request can then be sent using the function “SendDataRequest();”.

