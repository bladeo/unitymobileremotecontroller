﻿using UnityEngine;
using System.Collections;

public class Gyroscope : MonoBehaviour {

    // Use this for initialization
    public GameObject serverScriptHolder;

    void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
        if (serverScriptHolder.GetComponent<ServerScript>().enableGyro == true)
        {
            Vector3 gyroVector = serverScriptHolder.GetComponent<ServerScript>().gyroscopeData.gyroVector;
            Quaternion gyroAltitude = serverScriptHolder.GetComponent<ServerScript>().gyroscopeData.gyroAltitude;
        }
    }
}
