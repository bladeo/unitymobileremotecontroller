﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VectorRotation : MonoBehaviour
{
    private float initialYAngle = 0f;
    private float appliedGyroYAngle = 0f;
    private float calibrationYAngle = 0f;
    public GameObject serverScriptHolder;

    void Start()
    {
        initialYAngle = transform.eulerAngles.y;
    }

    void Update()
    {
        if (serverScriptHolder.GetComponent<ServerScript>().enableRotationVector == true)
        {
            ApplyGyroRotation();
            ApplyCalibration();
        }
    }

    void OnGUI()
    {
        if (serverScriptHolder.GetComponent<ServerScript>().enableRotationVector == true)
        {
            if (GUILayout.Button("Calibrate sensor", GUILayout.Width(115), GUILayout.Height(20)))
            {
                CalibrateYAngle();
            }
        }
    }

    public void CalibrateYAngle()
    {
        calibrationYAngle = appliedGyroYAngle - initialYAngle; // Offsets the y angle in case it wasn't 0 at edit time.
    }

    void ApplyGyroRotation()
    {
        transform.rotation = serverScriptHolder.GetComponent<ServerScript>().vectorData.rotation;
        transform.Rotate(0f, 0f, 180f, serverScriptHolder.GetComponent<ServerScript>().vectorData.spaceSelf); // Swap "handedness" of quaternion from gyro.
        transform.Rotate(90f, 180f, 0f, serverScriptHolder.GetComponent<ServerScript>().vectorData.spaceWorld); // Rotate to make sense as a camera pointing out the back of your device.
        appliedGyroYAngle = transform.eulerAngles.y; // Save the angle around y axis for use in calibration.
    }

    void ApplyCalibration()
    {
        transform.Rotate(0f, -calibrationYAngle, 0f, serverScriptHolder.GetComponent<ServerScript>().vectorData.spaceWorld); // Rotates y angle back however much it deviated when calibrationYAngle was saved.
    }
}
