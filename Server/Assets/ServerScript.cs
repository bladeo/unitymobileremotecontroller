﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;


public class ServerScript : MonoBehaviour
{
    //Server variables
    [HideInInspector]public bool isAtStartup = true;
    [HideInInspector]public bool deviceConnected = false;
    string ip;
    int currentPing;
    int numberOfPingSamples = 0;
    int pingAverage = 0;
    int totalPing = 0;

    //Tells processing classes if sensor is enabled
    [HideInInspector]public bool enableRotationVector = false;
    [HideInInspector]public bool enableAcc = false;
    [HideInInspector]public bool enableGyro = false;

    //Data storage class objects
    DataRequestMessage dataMessage = new DataRequestMessage();
    public AccData accelerometerData = new AccData();
    public VectorRotData vectorData = new VectorRotData();
    public GyroData gyroscopeData = new GyroData();
    public Button1Press button1Data = new Button1Press();
    public Button2Press button2Data = new Button2Press();
    TestPing pingData = new TestPing();

    //Enable or disable sensors at startup using inspector
    public bool startupVectorDataEnabled;
    public bool startupAccelerometerEnabled;
    public bool startupGyroEnabled;
    public bool startupButtonsEnabled;
    public bool startupPingEnabled;
    public int startupTargetFps;



    //Message Bases
    public class DataRequestMessage : MessageBase //Class to store data request info
    {
        public bool[] dataRequest = new bool[4];
        public int targetFps;
    }

    public class VectorRotData : MessageBase //Vector rotation data class
    {
        public Quaternion rotation;
        public Space spaceSelf;
        public Space spaceWorld;
    }

    public class AccData : MessageBase //Class to store device acc data
    {
        public Vector3 accVector;
    }

    public class GyroData : MessageBase //Class to store device gyroscope data
    {
        public Vector3 gyroVector;
        public Quaternion gyroAltitude;
    }

    public class Button1Press : MessageBase //Class to store button1 press
    {
        public bool button1pressed;
    }

    public class Button2Press : MessageBase //Class to store button2 press
    {
        public bool button2pressed;
    }

    public class TestPing : MessageBase //Class to test ping
    {
        public float time;
    }




    //Recieved message functions
    void OnServerConnect(NetworkMessage netMsg) //Server on connection message
    {
        Debug.Log("Device Connected");
        deviceConnected = true;
        SendDataRequest();
        if (startupPingEnabled)
        {
            InvokeRepeating("SendPing", 0, 1.0f);
        }
    }
    
    void OnServerDisconnect(NetworkMessage netMsg) //Server on disconnect message
    {
        deviceConnected = false;
        enableRotationVector = false;
        Debug.Log("Lost Device");
    }
    
    void VectorDataRecived(NetworkMessage netMsg) //Vector rotation data recieved
    {
        enableRotationVector = true;
        vectorData = netMsg.ReadMessage<VectorRotData>();
    }

    void AccDataRecived(NetworkMessage netMsg) //Accelerometer data recieved
    {
        enableAcc = true;
        accelerometerData = netMsg.ReadMessage<AccData>();
    }

    void GyroDataRecieved(NetworkMessage netMsg) //Gyroscope data recieved
    {
        enableGyro = true;
        gyroscopeData = netMsg.ReadMessage<GyroData>();
    }

    void Button1Recieved(NetworkMessage netMsg) //Button data recieved
    {
        button1Data = netMsg.ReadMessage<Button1Press>();
    }

    void Button2Recieved(NetworkMessage netMsg) //Button data recieved
    {
        button2Data = netMsg.ReadMessage<Button2Press>();
    }

    public void OnPingReturn(NetworkMessage netMsg) //On recieving a ping
    {
        pingData = netMsg.ReadMessage<TestPing>();
        currentPing = (int)((Time.time - pingData.time) * 1000);
        //totalPing += currentPing;
        //numberOfPingSamples += 1;
        //pingAverage = totalPing / numberOfPingSamples;
        //Debug.Log("Number of samples: " + numberOfPingSamples + " Ping: " + pingAverage);
    }


    //Message types (used to differ between messages)
    public class MyMsgType //Message type used for vector data
    {
        public static short IntMessage = MsgType.Highest + 1;
    };

    public class ReqDataMessageType //Message type used to request data
    {
        public static short IntMessage = MsgType.Highest + 2;
    };

    public class AccMsgType //Message type for acc data
    {
        public static short IntMessage = MsgType.Highest + 3;
    };

    public class GyroMsgType //Message type for gyro data
    {
        public static short IntMessage = MsgType.Highest + 4;
    };

    public class Button1MsgType //Message type for gyro data
    {
        public static short IntMessage = MsgType.Highest + 5;
    };

    public class Button2MsgType //Message type for gyro data
    {
        public static short IntMessage = MsgType.Highest + 6;
    };

    public class PingMsgType //Message type for ping
    {
        public static short IntMessage = MsgType.Highest + 7;
    };



    //Server functions
    public void SetupServer() // Create a server and listen on a port
    {
        NetworkServer.Listen(4444);
        isAtStartup = false;
        ip = Network.player.ipAddress;

        NetworkServer.RegisterHandler(MsgType.Connect, OnServerConnect);
        NetworkServer.RegisterHandler(MsgType.Disconnect, OnServerDisconnect);
        NetworkServer.RegisterHandler(MyMsgType.IntMessage, VectorDataRecived); //Vector Rotation Data
        NetworkServer.RegisterHandler(AccMsgType.IntMessage, AccDataRecived); //Accelerometer Rotation Data
        NetworkServer.RegisterHandler(GyroMsgType.IntMessage, GyroDataRecieved); //Gyro Data
        NetworkServer.RegisterHandler(Button1MsgType.IntMessage, Button1Recieved); //button1 Data
        NetworkServer.RegisterHandler(Button2MsgType.IntMessage, Button2Recieved); //button2 Data
        NetworkServer.RegisterHandler(PingMsgType.IntMessage, OnPingReturn); //On Recieving ping back
    }

    public void SendDataRequest() //Function to request data request
    {
        NetworkServer.SendToAll(ReqDataMessageType.IntMessage, dataMessage);
    }

    public void SendPing() //Returns the ping message
    {
        pingData.time = Time.time;
        NetworkServer.SendToAll(PingMsgType.IntMessage, pingData);
    }

    public void DisconnectServer() //Shuts the server down
    {
        NetworkServer.DisconnectAll();
        NetworkServer.Shutdown();
    }





    //Unity functions
    void Start()
    {
        Application.targetFrameRate = 60;
        dataMessage.dataRequest[0] = startupVectorDataEnabled; //index 1 = vector data
        dataMessage.dataRequest[1] = startupAccelerometerEnabled; //index 2 = accelerometer
        dataMessage.dataRequest[2] = startupGyroEnabled; //index 3 = gyroscope
        dataMessage.dataRequest[3] = startupButtonsEnabled; //index 4 = Buttons enabled
        dataMessage.targetFps = startupTargetFps;
        Application.targetFrameRate = startupTargetFps;
    }

    void Update()
    {
        if (isAtStartup)
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                SetupServer(); //Start server
            }
        }
    }

    void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.fontSize = 20;
        style.normal.textColor = Color.white;

        GUIStyle style2 = new GUIStyle();
        style2.fontSize = 20;
        style2.normal.textColor = Color.red;

        if (isAtStartup)
        {
            GUI.Label(new Rect(2, 10, 150, 100), "Press S to start server", style);
        }
        else if (deviceConnected == false)
        {
            GUI.Label(new Rect(2, 10, 150, 100), "IP: " + ip, style);
        }
        if (deviceConnected == false)
        {
            GUI.Label(new Rect(2, 33, 150, 100), "No device connected", style2);
        }
        if (startupPingEnabled && deviceConnected)
        {
            GUI.Label(new Rect(2, 20, 150, 100), "Ping = " + currentPing + "ms", style);
        }
    }
}

