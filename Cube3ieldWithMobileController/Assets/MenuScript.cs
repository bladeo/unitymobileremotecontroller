﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {
    public Text lastScore;
    public GameObject highScoreTxt;

    public GameObject gameTitle;
    public GameObject newGameBtn;
    public GameObject backPanel1;
    public GameObject backPanel2;
    public GameObject infoText;

    public GameObject highscoreBackPanel;
    public GameObject highscoreLogo;
    public GameObject newHighscoreButton;
    public GameObject newHighscoreText;



    // Use this for initialization
    void Start()
    {
        //PlayerPrefs.SetInt("Score", 0); //Clear Scores
        //PlayerPrefs.SetInt("HighScore", 0);

        highscoreBackPanel.SetActive(false);
        highscoreLogo.SetActive(false);
        newHighscoreButton.SetActive(false);
        newHighscoreText.SetActive(false);

        int intLastScore = PlayerPrefs.GetInt("Score");
        lastScore.text = "" + intLastScore;

        if (PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", PlayerPrefs.GetInt("Score"));
            highscoreBackPanel.SetActive(true);
            highscoreLogo.SetActive(true);
            newHighscoreButton.SetActive(true);
            newHighscoreText.GetComponent<Text>().text = "New Highscore: " + PlayerPrefs.GetInt("HighScore");
            newHighscoreText.SetActive(true);

            gameTitle.SetActive(false);
            newGameBtn.SetActive(false);
            backPanel1.SetActive(false);
            backPanel2.SetActive(false);
            infoText.SetActive(false);
            highScoreTxt.SetActive(false);

        }

        highScoreTxt.GetComponent<Text>().text = "Top Score: " + PlayerPrefs.GetInt("HighScore");

        int currentPlayCount = PlayerPrefs.GetInt("PlayCount");
        PlayerPrefs.SetInt("PlayCount", currentPlayCount + 1);

        if (PlayerPrefs.GetInt("PlayCount") == 5)
        {
            PlayerPrefs.SetInt("PlayCount", 0);
        }
    }
	   
    public void CloseButtonPress()
    {
        highscoreBackPanel.SetActive(false);
        highscoreLogo.SetActive(false);
        newHighscoreButton.SetActive(false);
        newHighscoreText.SetActive(false);

        gameTitle.SetActive(true);
        newGameBtn.SetActive(true);
        backPanel1.SetActive(true);
        backPanel2.SetActive(true);
        infoText.SetActive(true);
        highScoreTxt.SetActive(true);

    }

	// Update is called once per frame
	void Update () {
	
	}

    public void startGame()
    {
        SceneManager.LoadScene("Scene1");
    }

}
