﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CubeCollision : MonoBehaviour {

    // Use this for initialization
    public GameObject movementScriptObj;
    public GameObject serverScript;

    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnCollisionEnter(Collision collision)
    {
        serverScript.GetComponent<ServerScript>().DisconnectServer();
        PlayerPrefs.SetInt("Score", movementScriptObj.GetComponent<MovementScript>().score);
        SceneManager.LoadScene("Menu");
    }
}
