﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CubeBuilder : MonoBehaviour
{

    // Use this for initialization
    public GameObject movementScriptObject;
    public GameObject cube;
    public Material sky1;
    public Material sky2;
    public Material sky3;
    public Material lvl1Mat1;
    public Material lvl1Mat2;
    public Material lvl1Mat3;
    public Material lvl2Mat1;
    public Material lvl3Mat1;
    public Material lvl4Mat1;
    public GameObject serverScript;

    int y = 0;
    int placeCube = 0;
    List<GameObject> cubeList = new List<GameObject>();
    List<Material> mats = new List<Material>();
    int delay = 270;
    int Hx = 55;
    int Lx = 25;
    int fEvent = 0;
    int ytoEndEvent = 0;
    float EventxOffset = 0;
    int lowToAdd = 0;
    int highToAdd = 100;
    int lowTxRange = 0;
    int highTxRange = 3;
    int currentLvl = 0;
    int eventMat = 2;

    bool switchStyleOnce = true;
    int numberOfEvents = 0;

    void Start()
    {
        // Application.targetFrameRate = 60;
        RenderSettings.skybox = sky1;
        mats.Add(lvl1Mat1);
        mats.Add(lvl1Mat2);
        mats.Add(lvl1Mat3);
        mats.Add(lvl2Mat1);
        mats.Add(lvl3Mat1);
        mats.Add(lvl4Mat1);
    }

    void switchSyle(int lvl)
    {
        switch (lvl)
        {
            case 0:
                RenderSettings.skybox = sky1;
                lowTxRange = 0;
                highTxRange = 3;
                eventMat = 2;
                break;
            case 1:
                RenderSettings.skybox = sky2;
                lowTxRange = 3;
                highTxRange = 3;
                eventMat = 3;
                break;
            case 2:
                RenderSettings.skybox = sky3;
                lowTxRange = 4;
                highTxRange = 4;
                eventMat = 4;
                break;
            case 3:
                RenderSettings.skybox = sky3;
                lowTxRange = 5;
                highTxRange = 5;
                eventMat = 5;
                break;
        }
        int randMat = Random.Range(lowTxRange, highTxRange);
        for (int i = 0; i < cubeList.Count; i++)
        {
            cubeList[i].GetComponent<MeshRenderer>().material = mats[randMat];
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (serverScript.GetComponent<ServerScript>().deviceConnected == true)
        {
            if (delay > -300)
            {
                delay -= 1;
            }
            y = (int)transform.position.y + 40;
            if ((int)y % 450 == 0)
            {
                fEvent = 1;
                ytoEndEvent = (int)y + 200;
                Hx = 65;
                Lx = 35; ;
                EventxOffset = transform.position.x - 40;
                lowToAdd = 0;
                highToAdd = 79;
                switchStyleOnce = true;
            }
            if (fEvent == 1 && (int)y % ytoEndEvent == 0)
            {
                fEvent = 0;
                switchStyleOnce = true;
                numberOfEvents += 1;
                if (numberOfEvents % 2 == 0)
                {
                    if (switchStyleOnce)
                    {
                        currentLvl += 1;
                        if (currentLvl == 4)
                        {
                            currentLvl = 0;
                        }
                        switchSyle(currentLvl);
                        movementScriptObject.GetComponent<MovementScript>().IncreaseSpeed();
                        switchStyleOnce = false;
                    }
                }
            }
            if (placeCube == 4)
            {
                switch (fEvent)
                {
                    case 0:
                        for (int x = 0; x < 80; x++)
                        {
                            for (int z = 0; z < 80; z++)
                            {
                                int rand = 0;
                                rand = Random.Range(0, 50);

                                if (rand == 1)
                                {
                                    float xOffset = transform.position.x - 40;
                                    float zOffset = transform.position.z - 40;
                                    Vector3 pos = new Vector3(x + xOffset, y, z + zOffset);
                                    GameObject newcube = (GameObject)Instantiate(cube, pos, cube.gameObject.transform.rotation);
                                    int randMat = Random.Range(lowTxRange, highTxRange);
                                    newcube.GetComponent<MeshRenderer>().material = mats[randMat];
                                    cubeList.Add(newcube);
                                    if (delay < 0)
                                    {
                                        GameObject delCube = (GameObject)cubeList[0];
                                        Destroy(delCube.gameObject);
                                        cubeList.RemoveAt(0);
                                    }
                                }
                            }
                        }
                        placeCube = 0;
                        break;
                    case 1:
                        if (lowToAdd < 35)
                        {
                            lowToAdd += 1;
                            highToAdd -= 1;

                            for (int z = 0; z < 80; z++)
                            {
                                for (int x = 0; x < 80; x++)
                                {
                                    if (x == lowToAdd || x == highToAdd)
                                    {
                                        float zOffset = transform.position.z - 40;
                                        Vector3 pos = new Vector3(x + EventxOffset, y, z + zOffset);
                                        GameObject newcube = (GameObject)Instantiate(cube, pos, cube.gameObject.transform.rotation);
                                        newcube.GetComponent<MeshRenderer>().material = mats[eventMat];
                                        cubeList.Add(newcube);
                                        GameObject delCube = (GameObject)cubeList[0];
                                        Destroy(delCube.gameObject);
                                        cubeList.RemoveAt(0);
                                    }
                                }
                            }
                        }
                        else
                        {

                            for (int z = 0; z < 80; z++)
                            {
                                float zOffset = transform.position.z - 40;
                                Vector3 pos = new Vector3(Hx + EventxOffset, y, z + zOffset);
                                GameObject newcube = (GameObject)Instantiate(cube, pos, cube.gameObject.transform.rotation);
                                newcube.GetComponent<MeshRenderer>().material = mats[eventMat];
                                pos = new Vector3(Lx + EventxOffset, y, z + zOffset);
                                GameObject newcube1 = (GameObject)Instantiate(cube, pos, cube.gameObject.transform.rotation);
                                newcube1.GetComponent<MeshRenderer>().material = mats[eventMat];
                                cubeList.Add(newcube);
                                cubeList.Add(newcube1);
                                GameObject delCube = (GameObject)cubeList[0];
                                GameObject delCube1 = (GameObject)cubeList[1];
                                Destroy(delCube.gameObject);
                                Destroy(delCube1.gameObject);
                                cubeList.RemoveAt(0);
                                cubeList.RemoveAt(0);
                            }
                            if ((int)y > ytoEndEvent - 75)
                            {
                                Hx += 1;
                                Lx += 1;
                            }
                            else
                            {
                                Hx -= 1;
                                Lx -= 1;
                            }
                        }
                        placeCube = 0;
                        break;
                }
            }
            else
            {
                placeCube += 1;
            }

            if (delay == -150)
            {
                while (cubeList.Count > 4300)
                {
                    GameObject delCube = (GameObject)cubeList[0];
                    Destroy(delCube.gameObject);
                    cubeList.RemoveAt(0);
                }
            }
        }
    }
}