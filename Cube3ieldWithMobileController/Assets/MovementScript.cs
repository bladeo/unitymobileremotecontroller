﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MovementScript : MonoBehaviour
{

    // Use this for initialization
    public Text scoreLbl;
    public GameObject speedUpText;
    public GameObject speedUpImage;
    public GameObject serverScript;
    int delay = 100;
    public float smooth = 0.4f;
    public float newRotationx, newRotationy, newRotationz;
    public float sensitivity = 1;
    Vector3 currentAcceleration, initialAcceleration;
    float speed = 0.3f;
    public int score = 0;
    public int speedUpTxtTimer = 0;

    public void IncreaseSpeed()
    {
        speed += 0.09f;
        speedUpTxtTimer += 70;
        speedUpText.SetActive(true);
        speedUpImage.SetActive(true);
    }

    void Start()
    {
        speedUpText.SetActive(false);
        speedUpImage.SetActive(false);
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        initialAcceleration = Input.acceleration;
        currentAcceleration = Vector3.zero;
    }
    // Update is called once per frame
    void Update()
    {
        if (speedUpTxtTimer > -1)
        {
            speedUpTxtTimer -= 1;
        } else
        {
            speedUpText.SetActive(false);
            speedUpImage.SetActive(false);
        }

        score = (int)transform.position.y;
        if (delay < 0)
        {
            if (transform.position.y > 0)
            {
                scoreLbl.text = "" + score;
            }
            Vector3 position = transform.position;
            position.y += speed;
            transform.position = position;

            if (Input.GetKey("w"))
            {
                position = transform.position;
                position.z -= 0.3f;
                transform.position = position;
            }
            if (Input.GetKey("s"))
            {
                position = transform.position;
                position.z += 0.3f;
                transform.position = position;
            }
            if (Input.GetKey("a"))
            {
                position = transform.position;
                position.x -= 0.3f;
                transform.position = position;
            }
            if (Input.GetKey("d"))
            {
                position = transform.position;
                position.x += 0.3f;
                transform.position = position;
            }
            //if (Application.platform == RuntimePlatform.Android)
            //{
            //    currentAcceleration = Vector3.Lerp(currentAcceleration, Input.acceleration - initialAcceleration, Time.deltaTime / smooth); //PreProcess Results
            //    newRotationx = Mathf.Clamp(currentAcceleration.x * sensitivity / 5, -0.2f, 0.2f);
            //    newRotationy = Mathf.Clamp(currentAcceleration.y * sensitivity / 5, -0.2f, 0.2f);
            //    transform.Translate(newRotationx, newRotationy, 0);
            //}
        }
        else if (serverScript.GetComponent<ServerScript>().deviceConnected == true)
        {
            delay -= 1;
        }
    }
}
