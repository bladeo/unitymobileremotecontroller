﻿using UnityEngine;
using System.Collections;

public class AccelerometerScript : MonoBehaviour
{

    // Use this for initialization
    float smooth = 0.4f;
    float newRotationx, newRotationy, newRotationz;
    float sensitivity = 1;
    public GameObject serverScriptHolder;
    Vector3 currentAcceleration;

    void Start()
    {
        currentAcceleration = Vector3.zero;
    }
    // Update is called once per frame
    void Update()
    {
        if (serverScriptHolder.GetComponent<ServerScript>().enableAcc)
        {
            Vector3 newInput = serverScriptHolder.GetComponent<ServerScript>().accelerometerData.accVector;
            currentAcceleration = Vector3.Lerp(currentAcceleration, newInput, Time.deltaTime / smooth); //PreProcess Results
            newRotationx = Mathf.Clamp(currentAcceleration.x * sensitivity / 5, -0.4f, 0.4f);
            newRotationy = Mathf.Clamp(currentAcceleration.y * sensitivity / 5, -0.4f, 0.4f);
            newRotationz = Mathf.Clamp(currentAcceleration.z * sensitivity / 5, -0.4f, 0.4f);
            transform.Translate(newRotationx, newRotationy, 0); //Update z to newRotationz for z axis
        }
    }
}
