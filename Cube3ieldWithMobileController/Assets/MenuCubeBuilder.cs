﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuCubeBuilder : MonoBehaviour {

    // Use this for initialization
    public Material sky1;
    public Material lvl1Mat1;
    public Material lvl1Mat2;
    public Material lvl1Mat3;
    List<Material> mats = new List<Material>();
    List<GameObject> cubeList = new List<GameObject>();
    public GameObject cube;
    int placeCube = 0;
    float y;

    void Start () {
        RenderSettings.skybox = sky1;
        mats.Add(lvl1Mat1);
        mats.Add(lvl1Mat2);
        mats.Add(lvl1Mat3);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = transform.position;
        position.y += 0.1f;
        transform.position = position;

        y = (int)transform.position.y + 40;
        if (placeCube == 4)
        {
            for (int x = 0; x < 80; x++)
            {
                for (int z = 0; z < 80; z++)
                {
                    int rand = 0;
                    rand = Random.Range(0, 500);

                    if (rand == 1)
                    {
                        float xOffset = transform.position.x - 40;
                        float zOffset = transform.position.z - 40;
                        Vector3 pos = new Vector3(x + xOffset, y, z + zOffset);
                        GameObject newcube = (GameObject)Instantiate(cube, pos, cube.gameObject.transform.rotation);
                        int randMat = Random.Range(0, 3);
                        newcube.GetComponent<MeshRenderer>().material = mats[randMat];
                        cubeList.Add(newcube);
                        if (cubeList.Count > 1500)
                        {
                            GameObject delCube = (GameObject)cubeList[0];
                            Destroy(delCube.gameObject);
                            cubeList.RemoveAt(0);
                        }
                    }
                }
            }
            placeCube = 0;
        }
        placeCube += 1;
    }
}
