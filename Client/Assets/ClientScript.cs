﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class ClientScript : MonoBehaviour
{
    public GameObject uiHandlerHolder;
    [HideInInspector] public bool isAtStartup = true;
    [HideInInspector] public bool connected = false;

    //Collects data if enabled
    bool enableVectorData = false;
    bool enableaccData = false;
    bool enableGyroData = false;
    [HideInInspector] public bool enableButtons = false;

    //Client variables
    NetworkClient myClient;
    string ip;

    //Data storage class objects
    VectorRotData vectorData = new VectorRotData();
    DataRequestMessage dataMessasge = new DataRequestMessage();
    AccData accelerometerData = new AccData();
    GyroData gyroscopeData = new GyroData();
    Button1Press button1Data = new Button1Press();
    Button2Press button2Data = new Button2Press();
    TestPing pingData = new TestPing();


    //Message Bases
    public class AccData : MessageBase //Class to store device acc data
    {
        public Vector3 accVector;
    }

    public class VectorRotData : MessageBase //Class to store device vector rotation data
    {
        public Quaternion rotation;
        public Space spaceSelf;
        public Space spaceWorld;
    }

    public class DataRequestMessage : MessageBase //Class to store data request info
    {
        public bool[] dataRequest = new bool[4];
        public int targetFps;
    }

    public class GyroData : MessageBase //Class to store device gyroscope data
    {
        public Vector3 gyroVector;
        public Quaternion gyroAltitude;
    }

    public class Button1Press : MessageBase //Class to store button1 press
    {
        public bool button1pressed;
    }
    public class Button2Press : MessageBase //Class to store button2 press
    {
        public bool button2pressed;
    }
    public class TestPing : MessageBase //Class to test ping
    {
        public float time;
    }



    //Recieved message functions
    public void OnConnected(NetworkMessage netMsg) // Client on connection message
    {
        Debug.Log("Connected to server");
        connected = true;
    }

    public void OnDisconnect(NetworkMessage netMsg) // Client on disconnect message
    {
        Debug.Log("Disconnect from server restart client when server is on");
        connected = false;
        enableVectorData = false;
        enableaccData = false;
        enableButtons = false;
        enableGyroData = false;
    }

    public void OnDataRequest(NetworkMessage netMsg) //On recieving a data request
    {
        dataMessasge = netMsg.ReadMessage<DataRequestMessage>();
        if (dataMessasge.dataRequest[0] == true)
        {
            enableVectorData = true;
        }
        if (dataMessasge.dataRequest[1] == true)
        {
            enableaccData = true;
        }
        if (dataMessasge.dataRequest[2] == true)
        {
            enableGyroData = true;
        }
        if (dataMessasge.dataRequest[3] == true)
        {
            enableButtons = true;
        }
        Application.targetFrameRate = dataMessasge.targetFps;
    }

    public void OnPing(NetworkMessage netMsg) //On recieving a ping
    {
        pingData = netMsg.ReadMessage<TestPing>();
        SendPing();
    }


    //Message types (used to differ between messages)
    public class MyMsgType //Message type for vector data message
    {
        public static short IntMessage = MsgType.Highest + 1;
    };

    public class AccMsgType //Message type for acc data
    {
        public static short IntMessage = MsgType.Highest + 3;
    };

    public class ReqDataMessageType //Message type used to request data
    {
        public static short IntMessage = MsgType.Highest + 2;
    };

    public class GyroMsgType //Message type for gyro data
    {
        public static short IntMessage = MsgType.Highest + 4;
    };

    public class Button1MsgType //Message type for gyro data
    {
        public static short IntMessage = MsgType.Highest + 5;
    };

    public class Button2MsgType //Message type for gyro data
    {
        public static short IntMessage = MsgType.Highest + 6;
    };

    public class PingMsgType //Message type for ping
    {
        public static short IntMessage = MsgType.Highest + 7;
    };



    //Client functions
    public void SetupClient() // Create a client and connect to the server port
    {
        ip = uiHandlerHolder.GetComponent<UiHandler>().ReturnIP();
        myClient = new NetworkClient();
        myClient.RegisterHandler(MsgType.Connect, OnConnected);
        myClient.RegisterHandler(MsgType.Disconnect, OnDisconnect);
        myClient.RegisterHandler(ReqDataMessageType.IntMessage, OnDataRequest);
        myClient.RegisterHandler(PingMsgType.IntMessage, OnPing);
        myClient.Connect(ip, 4444); //127.0.0.1
        isAtStartup = false;
    }

    public void SendVectorRotation() //Sends Vector rotation data
    {
        myClient.Send(MyMsgType.IntMessage, vectorData);
    }

    public void SendAccData() //Sends acc data
    {
        myClient.Send(AccMsgType.IntMessage, accelerometerData);
    }

    public void SendGyroData() //Sends gyroscore data
    {
        myClient.Send(GyroMsgType.IntMessage, gyroscopeData);
    }

    public void SendButton1Press(bool pressed) //Sends button1 press
    {
        button1Data.button1pressed = pressed;
        myClient.Send(Button1MsgType.IntMessage, button1Data);
    }

    public void SendButton2Press(bool pressed) //Sends button2 press
    {
        button2Data.button2pressed = pressed;
        myClient.Send(Button2MsgType.IntMessage, button2Data);
    }

    public void SendPing() //Returns the ping message
    {
        myClient.Send(PingMsgType.IntMessage, pingData);
    }




    //Unity functions
    void Start()
    {
        Input.gyro.enabled = true;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
    void Update()
    {
        if (enableVectorData == true)
        {
            vectorData.rotation = Input.gyro.attitude;
            vectorData.spaceSelf = Space.Self;
            vectorData.spaceWorld = Space.World;
            SendVectorRotation();
        }
        if (enableaccData == true)
        {
            accelerometerData.accVector = Input.acceleration;
            SendAccData();
        }
        if (enableGyroData == true)
        {
            gyroscopeData.gyroVector = Input.gyro.rotationRateUnbiased;
            gyroscopeData.gyroAltitude = Input.gyro.attitude;
            SendGyroData();
        }
    }
}

