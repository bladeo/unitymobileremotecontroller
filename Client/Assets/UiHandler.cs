﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UiHandler : MonoBehaviour {
    public GameObject clientScriptHolder;
    public Text textInput;
    public GameObject inputField;
    public GameObject startClientButton;
    public GameObject button1;
    public GameObject button2;
    bool button1bool = false;
    bool button2bool = false;
    bool button1Stopped = true;
    bool button2Stopped = true;

    // Use this for initialization
    void Start () {
        button1.SetActive(false);
        button2.SetActive(false);
	}

    public string ReturnIP()
    {
        return textInput.text;
    }
	
	// Update is called once per frame
	void Update () {
	    if (button1bool)
        {
            clientScriptHolder.GetComponent<ClientScript>().SendButton1Press(true);
        } else if (button1Stopped == false)
        {
            clientScriptHolder.GetComponent<ClientScript>().SendButton1Press(false);
            button1Stopped = true;
        }
        if (button2bool)
        {
            clientScriptHolder.GetComponent<ClientScript>().SendButton2Press(true);
        }
        else if (button2Stopped == false)
        {
            clientScriptHolder.GetComponent<ClientScript>().SendButton2Press(false);
            button2Stopped = true;
        }
    }

    void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.fontSize = 30;
        style.normal.textColor = Color.white;

        GUIStyle style2 = new GUIStyle();
        style2.fontSize = 30;
        style2.normal.textColor = Color.red;

        if (clientScriptHolder.GetComponent<ClientScript>().isAtStartup)
        {
            GUI.Label(new Rect(2, 70, 150, 100), "Client Not Started", style);
        }
        else
        {
            GUI.Label(new Rect(2, 100, 150, 100), ReturnIP());
        }
        if (clientScriptHolder.GetComponent<ClientScript>().connected)
        {
            GUI.Label(new Rect(2, 70, 150, 100), "Connected to server", style);
            inputField.SetActive(false);
            startClientButton.SetActive(false);
        }
        else if (!clientScriptHolder.GetComponent<ClientScript>().isAtStartup && !clientScriptHolder.GetComponent<ClientScript>().isAtStartup)
        {
            GUI.Label(new Rect(2, 70, 150, 100), "Disconnected from server please restart client when server is on", style2);
            inputField.SetActive(true);
            startClientButton.SetActive(true);
        }

        if (clientScriptHolder.GetComponent<ClientScript>().enableButtons)
        {
            button1.SetActive(true);
            button2.SetActive(true);
        } else
        {
            button1.SetActive(false);
            button2.SetActive(false);
        }
    }

    public void button1Pressed()
    {
        button1bool = true;
    }
    public void button1Released()
    {
        button1bool = false;
        button1Stopped = false; //Ensure only 1 false message is sent
    }
    public void button2Pressed()
    {
        button2bool = true;
    }
    public void button2Released()
    {
        button2bool = false;
        button2Stopped = false; //Ensure only 1 false message is sent
    }
}
